﻿using System.CommandLine;
using System.Text;
using System.Text.Json;
using TaskCore;

namespace VkcpNmpdEmployeeToVehicleTransfer;

public class Payload : Runnable
{
    public override string PayloadName => "VkcpNmpdEmployeeToVehicleTransfer";
    private const int ClientTypeScheduler = 6;

    public override Command GetCommands(Option moduleOption)
    {
        var instanceOption = Helper.CreateOption<string>(
            "--instance_code",
            "-i",
            "TR_VkcpNmpdEmployeeToVehicleTransfer_InstanceCode",
            "Instance code");
        var tokenOption = Helper.CreateOption<string>(
            "--token",
            "-t",
            "TR_VkcpNmpdEmployeeToVehicleTransfer_Token",
            "Token for auth");
        var fromUrlOption = Helper.CreateOption<string>(
            "--from_url",
            "-f",
            "TR_VkcpNmpdEmployeeToVehicleTransfer_FromUrl",
            "Endpoint to fetch data");
        var toUrlOption = Helper.CreateOption<string>(
            "--destination_url",
            "-d",
            "TR_VkcpNmpdEmployeeToVehicleTransfer_DestinationUrl",
            "Endpoint to save data");

        var rootCommand = new RootCommand { moduleOption, instanceOption, tokenOption, fromUrlOption, toUrlOption };

        rootCommand.SetHandler(
            Helper.RunOrLogAsync<string, string, string, string>(this.Run),
            instanceOption,
            tokenOption,
            fromUrlOption,
            toUrlOption);

        return rootCommand;
    }

    private async Task Run(string instanceCode, string token, string fromUrl, string toUrl)
    {
        var response = await this.Fetch(instanceCode, token, fromUrl);
        await this.Save(response.Result, instanceCode, token, toUrl);
    }

    private async Task<Response<NmpdEmployeeToVehicleListResultGet<NmpdEmployeeToVehicleList>>> Fetch(string instanceCode, string token, string fromUrl)
    {
        var result = await this.PostAsync<Response<NmpdEmployeeToVehicleListResultGet<NmpdEmployeeToVehicleList>>>(fromUrl, instanceCode, token);
        if (result?.Error is not null)
        {
            throw new Exception("Fetch failed ");
        }
        Console.WriteLine("====================Fetch done".PadRight(70, '='));
        return result!;
    }

    private async Task Save(NmpdEmployeeToVehicleListResultGet<NmpdEmployeeToVehicleList> @params, string instanceCode, string token, string toUrl)
    {
        var result = await this.PostAsync<Response<NmpdEmployeeToVehicleListSaveResult>>(toUrl, instanceCode, token, @params);
        if (result?.Error is not null)
        {
            throw new Exception("Save failed");
        }
        Console.WriteLine("====================Save done".PadRight(70, '='));
    }
    
    private async Task<T?> PostAsync<T>(string url, string instanceCode, string token, object? args = null)
    {
        using var client = new HttpClient();
        var parameters = JsonSerializer.Serialize(new
        {
            Args = args,
            Token = token,
            ClientType = Payload.ClientTypeScheduler,
            InstCode = instanceCode
        });
        var content = new StringContent(parameters, Encoding.UTF8, "application/json");
        var response = await client.PostAsync(url, content);
        response.EnsureSuccessStatusCode();
        var json = await response.Content.ReadAsStringAsync();
        var @object = JsonSerializer.Deserialize<T>(json);
        return @object;
    }
}

public record NmpdEmployeeToVehicleList(string PersonCode, string DeviceId);
public record NmpdEmployeeToVehicleListResultGet<T>(List<T> NmpdEmployeeToVehicleList, List<string> EmployeeWithSession);
public record NmpdEmployeeToVehicleListSaveResult(bool Success);
public record Response<T>(T Result, string? Error, int? ErrorId, int Type, int BLProcessTime, int? SQLProcessTime, int? ResponseDeserializationTime, string? InnerException);