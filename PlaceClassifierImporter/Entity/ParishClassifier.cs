﻿namespace PlaceClassifierImporter.Entity;

public record ParishClassifier : Classifier<ParishClassifier>
{
    public override string TableName => "CLS_Parish";
    public string? CountyCode { get; set; }

    public override IEqualityComparer<ParishClassifier> Comparer() => new EqualityComparer();
    
    private sealed class EqualityComparer : IEqualityComparer<ParishClassifier>
    {
        public bool Equals(ParishClassifier? x, ParishClassifier? y)
        {
            if (object.ReferenceEquals(x, y)) return true;
            if (object.ReferenceEquals(x, null)) return false;
            if (object.ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Code == y.Code &&
                   x.DisplayName == y.DisplayName &&
                   x.CountyCode == y.CountyCode;
        }

        public int GetHashCode(ParishClassifier obj)
        {
            return HashCode.Combine(obj.Code, obj.DisplayName, obj.CountyCode);
        }
    }
}