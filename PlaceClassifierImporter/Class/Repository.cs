﻿using System.Data;
using System.Data.SqlClient;
using Dapper;
using Npgsql;

namespace PlaceClassifierImporter.Class;

public class Repository
{
    private readonly string _sourceConnectionString;
    private readonly string _destinationConnectionString;

    protected Repository(string sourceConnectionString, string destinationConnectionString)
    {
        this._sourceConnectionString = sourceConnectionString;
        this._destinationConnectionString = destinationConnectionString;
    }

    private const string UpdateVersionsQuery = "EXECUTE [mfw].[DataObjectVersionUpdate] @Schema = N'GIS', @ObjectName = N'{0}';";

    public void UpdateVersioning(string table)
    {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(Repository.UpdateVersionsQuery, table));
    }

    protected IDbConnection CreateOpenSourceConnection()
    {
        var connection = new NpgsqlConnection(this._sourceConnectionString);
        connection.Open();
        return connection;
    }

    protected IDbConnection CreateOpenDestinationConnection()
    {
        var connection = new SqlConnection(this._destinationConnectionString);
        connection.Open();
        return connection;
    }
}