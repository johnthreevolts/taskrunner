﻿namespace PlaceClassifierImporter.Entity;

public record StreetClassifier : Classifier<StreetClassifier>
{
    public override string TableName => "CLS_Street";
    public string? CityCode { get; set; }
    public string? VillageCode { get; set; }
    public string? ParishCode { get; set; }
    public string? CountyCode { get; set; }
    
    public override StreetClassifier Enrich(StreetClassifier classifier, Dictionary<string, StreetClassifier> source)
    {
        if (classifier.ParishCode is null && classifier.VillageCode is not null)
        {
            classifier.ParishCode = source[classifier.VillageCode]?.ParishCode ?? null;
        }
        if (classifier.CountyCode is null && (classifier.CityCode is not null || classifier.ParishCode is not null))
        {
            if (classifier.CityCode is not null && source.TryGetValue(classifier.CityCode, out var cityClassifier))
            {
                classifier.CountyCode = cityClassifier.CountyCode;
            }
            else if (classifier.ParishCode is not null &&source.TryGetValue(classifier.ParishCode, out var parishCodeClassifier))
            {
                classifier.CountyCode = parishCodeClassifier.CountyCode;
            }
        }
        
        return classifier;
    }

    public override IEqualityComparer<StreetClassifier> Comparer() => new EqualityComparer();
    
    private sealed class EqualityComparer : IEqualityComparer<StreetClassifier>
    {
        public bool Equals(StreetClassifier? x, StreetClassifier? y)
        {
            if (object.ReferenceEquals(x, y)) return true;
            if (object.ReferenceEquals(x, null)) return false;
            if (object.ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Code == y.Code &&
                   x.DisplayName == y.DisplayName &&
                   x.CityCode == y.CityCode &&
                   x.VillageCode == y.VillageCode &&
                   x.ParishCode == y.ParishCode &&
                   x.CountyCode == y.CountyCode;
        }

        public int GetHashCode(StreetClassifier obj)
        {
            return HashCode.Combine(obj.Code, obj.DisplayName, obj.CityCode, obj.VillageCode, obj.ParishCode, obj.CountyCode);
        }
    }
}