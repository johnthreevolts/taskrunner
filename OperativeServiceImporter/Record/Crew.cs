﻿namespace OperativeServiceImporter.Record;

public record Crew(string Name)
{
    public Crew() : this(string.Empty) { }
}