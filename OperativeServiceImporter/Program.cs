using System.CommandLine;
using OperativeServiceImporter.Class;
using OperativeServiceImporter.Record;
using TaskCore;

namespace OperativeServiceImporter;

public class Payload : Runnable
{
    #region Fields

    private int _actualTable;
    
    #endregion
    #region Properties
    
    public override string PayloadName => "OperativeServiceImporter";
    private string AuthorityTable => "Authority" + (this._actualTable == 1 ? "A" : "B");
    private string InactiveAuthorityTable => "Authority" + (this._actualTable == 1 ? "B" : "A");
    private string AuthorityHistoryTable => "Authority" + (this._actualTable == 1 ? "A" : "B") + "History";
    private string InactiveAuthorityHistoryTable => "Authority" + (this._actualTable == 1 ? "B" : "A") + "History";
    
    #endregion

    public override Command GetCommands(Option moduleOption)
    {
        var connectionStringOption = Helper.CreateOption<string>(
            "--connection_string",
            "-c",
            "TR_MS_SQL_ConnectionString",
            "Connection string of VKCP DB");

        var instanceOption = Helper.CreateOption<string>(
            "--operating_service",
            "-o",
            "TR_OperativeServiceImporter_OperatingService",
            "Operating service: VP or other");

        var dryRunOption = Helper.CreateOption<bool>(
            "--dry_run",
            "-d",
            "TR_OperativeServiceImporter_DryRun",
            "Only show validations, but do not affect data in DB",
            false);

        var rootCommand = new RootCommand { moduleOption, connectionStringOption, instanceOption, dryRunOption };

        rootCommand.SetHandler(
            Helper.RunOrLog<string, string, bool>(this.Run),
            connectionStringOption,
            instanceOption,
            dryRunOption);

        return rootCommand;
    }

    private void Run(string connectionString, string operatingService, bool isDryRun)
    {
        var repo = new Repository(connectionString);
        
        this._actualTable = repo.GetTableSetting() switch
        {
            { } value => value,
            null => throw new GracefulException("❌  Wrong setting came from DB, expected 1 or 2.")
        };
        
        operatingService = repo.GetOperatingService(operatingService) switch
        {
            { } value => value,
            null => throw new GracefulException($"❌  Wrong operating service, expected VP or other, got: {operatingService}.")
        };
        
        var previewUnits = repo.GetPreviewUnits(operatingService) switch
        {
            { } value when !value.Any() => throw new GracefulException("⚠️  Nothing to import from [mfw].[CLS_UnitPreview]."),
            { } value => value.ToDictionary(k => k.Id)
        };


        if (!this.Validate(previewUnits.Values, repo))
        {
            throw new GracefulException();
        }

        if (!isDryRun)
        {
            repo.TruncateAuthorityTablePair(this.InactiveAuthorityTable, this.InactiveAuthorityHistoryTable);
            repo.CopyToInactiveAuthorityHistoryTable(this.AuthorityHistoryTable, this.InactiveAuthorityHistoryTable);
            repo.CopyOtherServicesToInactiveAuthorityTable(this.AuthorityTable, this.InactiveAuthorityTable, operatingService);
            
            var actualUnits = repo.GetUnits(this.AuthorityTable, operatingService).ToDictionary(k => k.Id);
            this.InsertNewUnits(previewUnits, actualUnits, repo);
            this.UpdateChangedUnits(previewUnits, actualUnits, repo);
            this.InsertDeletedAndUnchangedUnits(previewUnits, actualUnits, repo);

            repo.UpdateUnitPreviewImportDate(operatingService);
            repo.SwitchActualTablePair(this._actualTable ^ 3);
        }
    }

    private bool Validate(IReadOnlyCollection<Unit> previewUnits, Repository repo)
    {
        if (!previewUnits.Any()) return true;
        
        if (Validator.ValidateTimeOverlap(previewUnits, out var errors))
        {
            Console.WriteLine(string.Join(Environment.NewLine, errors) + Environment.NewLine);
            return false;
        }
        
        if (Validator.ValidateDuplicateWithTimeOverlap(previewUnits, out errors))
        {
            Console.WriteLine(string.Join(Environment.NewLine, errors) + Environment.NewLine);
            return false;
        }
        
        var datePairs = Payload.ExtractDateIntervals(previewUnits);
        
        foreach (var pair in datePairs)
        {
            var intervalUnits = previewUnits.Where(Payload.DateIntervalPredicate(pair)).ToList();
            errors.AddRange(Validator.ValidateTree(intervalUnits, pair.ValidFrom,pair.ValidUntil));
        }
        if (errors.Any())
        {
            Console.WriteLine(string.Join(Environment.NewLine, errors) + Environment.NewLine);
            return false;
        }
        
        var warnings = new List<string>();
        foreach (var pair in datePairs)
        {
            var range = previewUnits.Where(Payload.DateIntervalPredicate(pair)).ToList();

            warnings.AddRange(Validator.ValidateUserProfiles(range, repo.GetDetachedUserProfiles().ToList()));
            warnings.AddRange(Validator.ValidateNumberGenerators(range, repo.GetDetachedNumberGenerators().ToList()));
            warnings.AddRange(Validator.ValidateEmergencyServiceVehicles(range, repo.GetDetachedEmergencyServiceVehicles().ToList()));
            warnings.AddRange(Validator.ValidateCrews(range, repo.GetDetachedCrews().ToList()));
            warnings.AddRange(Validator.ValidateOperativeServiceEmployees(range, repo.GetDetachedOperativeServiceEmployees().ToList()));
        }
        
        if (warnings.Any())
        {
            Console.WriteLine(string.Join(Environment.NewLine, warnings) + Environment.NewLine);
        }
        
        return true;
    }

    private void InsertNewUnits(Dictionary<long, Unit> previewUnits, Dictionary<long, Unit> actualUnits, Repository repo)
    {
        var newUnits = previewUnits.Keys
            .Except(actualUnits.Keys)
            .Select(k => previewUnits[k])
            .ToList();
        
        repo.InsertIntoInactiveAuthority(newUnits, this.InactiveAuthorityTable);
    }

    private void UpdateChangedUnits(Dictionary<long, Unit> previewUnits, Dictionary<long, Unit> actualUnits, Repository repo)
    {
        var changedUnits = actualUnits.Keys.Intersect(previewUnits.Keys)
            .Where(code => !actualUnits[code].Equals(previewUnits[code]))
            .ToList();
        var changedPreviewUnits = changedUnits.Select(k => previewUnits[k]).ToList();
        var changedActualUnits = changedUnits.Select(k => actualUnits[k]).ToList();

        repo.InsertIntoInactiveAuthority(changedPreviewUnits, this.InactiveAuthorityTable);
        repo.InsertIntoInactiveAuthorityHistory(changedActualUnits, this.InactiveAuthorityHistoryTable);
    }

    private void InsertDeletedAndUnchangedUnits(Dictionary<long, Unit> previewUnits, Dictionary<long, Unit> actualUnits, Repository repo)
    {
        var deletedUnitKeys = actualUnits.Keys
            .Except(previewUnits.Keys)
            .ToList();
        var changedUnitKeys = actualUnits.Keys.Intersect(previewUnits.Keys)
            .Where(code => !actualUnits[code].Equals(previewUnits[code]))
            .ToList();
        var oldUnits = actualUnits.Keys
            .Except(changedUnitKeys)
            .Except(deletedUnitKeys)
            .Select(k => actualUnits[k])
            .ToList();
        var deletedUnits = deletedUnitKeys
            .Select(k => actualUnits[k] with { EffectiveEndDate = DateTime.UtcNow})
            .ToList();

        repo.InsertIntoInactiveAuthority(oldUnits, this.InactiveAuthorityTable);
        repo.InsertIntoInactiveAuthority(deletedUnits, this.InactiveAuthorityTable);
    }

    private static Func<Unit, bool> DateIntervalPredicate((DateTime ValidFrom, DateTime ValidUntil) dates)
    {
        return unit =>
            (!unit.ValidFrom.HasValue && dates.ValidFrom == DateTime.MinValue) ||
            (!unit.ValidUntil.HasValue && dates.ValidUntil == DateTime.MaxValue) ||
            (unit.ValidFrom >= dates.ValidFrom && unit.ValidFrom <= dates.ValidUntil) ||
            (unit.ValidUntil >= dates.ValidFrom && unit.ValidUntil <= dates.ValidUntil);
    }

    private static List<(DateTime ValidFrom, DateTime ValidUntil)> ExtractDateIntervals(IEnumerable<Unit> previewUnits)
    {
        var dates = previewUnits
            .SelectMany(up => new[] { up.ValidFrom, up.ValidUntil })
            .Where(date => date.HasValue)
            .Cast<DateTime>()
            .Union(new List<DateTime> { DateTime.MinValue, DateTime.MaxValue })
            .OrderBy(d => d)
            .Where(d =>
                d == DateTime.MinValue ||
                d == DateTime.MaxValue ||
                d > DateTime.Today)
            .ToList();

        var datePairs = new List<(DateTime ValidFrom, DateTime ValidUntil)>();

        for (var i = 0; i < dates.Count - 1; i++)
        {
            datePairs.Add((dates[i], dates[i + 1]));
        }

        return datePairs;
    }
}