namespace OperativeServiceImporter.Record;

public record Unit(
    long Id,
    string Code,
    string DisplayName,
    byte Language,
    bool IsDeleted,
    string InstitutionCode,
    string InstitutionDisplayName,
    string Fullname,
    string Address,
    string EMail,
    string CellNumber,
    string FaxNumber,
    string Hierarchy,
    string HasLocation,
    string UrCode,
    string VissCode,
    DateTime? ValidFrom,
    DateTime? ValidUntil,
    string ParentCode,
    DateTime? EffectiveEndDate)
{
    public Unit() : this(
        default,
        string.Empty,
        string.Empty,
        default,
        default,
        string.Empty,
        string.Empty,
        string.Empty,
        string.Empty,
        string.Empty,
        string.Empty,
        string.Empty,
        string.Empty,
        string.Empty,
        string.Empty,
        string.Empty,
        default,
        default,
        string.Empty,
        default) { }

    public virtual bool Equals(Unit? other)
    {
        if (object.ReferenceEquals(this, other))
            return true;
        
        if (other is null)
            return false;
        
        return this.DisplayName == other.DisplayName &&
               this.Language == other.Language &&
               this.IsDeleted == other.IsDeleted &&
               this.InstitutionCode == other.InstitutionCode &&
               this.InstitutionDisplayName == other.InstitutionDisplayName &&
               this.Fullname == other.Fullname &&
               this.Address == other.Address &&
               this.EMail == other.EMail &&
               this.CellNumber == other.CellNumber &&
               this.FaxNumber == other.FaxNumber &&
               this.Hierarchy == other.Hierarchy &&
               this.HasLocation == other.HasLocation &&
               this.UrCode == other.UrCode &&
               this.VissCode == other.VissCode &&
               this.ValidFrom == other.ValidFrom &&
               this.ValidUntil == other.ValidUntil &&
               this.ParentCode == other.ParentCode &&
               this.EffectiveEndDate == other.EffectiveEndDate;
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = this.Id.GetHashCode();
            hashCode = (hashCode * 397) ^ (this.DisplayName != null ? this.DisplayName.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.Language != null ? this.Language.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.IsDeleted != null ? this.IsDeleted.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.InstitutionCode != null ? this.InstitutionCode.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.InstitutionDisplayName != null ? this.InstitutionDisplayName.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.Fullname != null ? this.Fullname.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.Address != null ? this.Address.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.EMail != null ? this.EMail.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.CellNumber != null ? this.CellNumber.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.FaxNumber != null ? this.FaxNumber.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.Hierarchy != null ? this.Hierarchy.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.HasLocation != null ? this.HasLocation.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.UrCode != null ? this.UrCode.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.VissCode != null ? this.VissCode.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.ValidFrom != null ? this.ValidFrom.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.ValidUntil != null ? this.ValidUntil.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.ParentCode != null ? this.ParentCode.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.EffectiveEndDate != null ? this.EffectiveEndDate.GetHashCode() : 0);
            return hashCode;
        }
    }
}