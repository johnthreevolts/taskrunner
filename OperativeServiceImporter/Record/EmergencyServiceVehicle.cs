﻿namespace OperativeServiceImporter.Record;

public record EmergencyServiceVehicle(string DisplayName)
{
    public EmergencyServiceVehicle() : this(string.Empty) { }
}