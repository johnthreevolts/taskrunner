﻿using Dapper;
using PlaceClassifierImporter.Entity;
using TaskCore;

namespace PlaceClassifierImporter.Class;

public class RoadRepository : Repository, IClassifierRepository<RoadClassifier>
{
    public RoadRepository(string sourceConnectionString, string destinationConnectionString): base(sourceConnectionString, destinationConnectionString) {}

    #region Postgres fetch queries

    private const string FetchSourceDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT
            CAST(c.kods as text)                        AS Code,
            c.nosaukums                                 AS DisplayName,
            CASE WHEN vkur_tips = 105 THEN vkur_cd END  AS ParishCode,
            CASE WHEN vkur_tips = 113 THEN vkur_cd END  AS CountyCode
        FROM varis.iedalijums c
        WHERE c.tips_cd = 117
            AND c.statuss='EKS';
        """;

    private const string FetchSourceDataHierarchyQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT 
            CAST(kods as text)                          AS Code,
            nosaukums                                   AS DisplayName,
            CASE WHEN vkur_tips = 113 THEN vkur_cd END  AS CountyCode
        FROM varis.iedalijums
        WHERE statuss = 'EKS' AND tips_cd IN (105);
        """;

    #endregion

    #region MSSQL fetch queries

    private const string FetchDestinationDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT Id, Code, DisplayName, CountyCode, ParishCode FROM [GIS].[{0}] WITH (NOLOCK) WHERE IsDeleted = 0;
        """;

    #endregion

    #region Update queries

    private const string MarkForDeleteQuery =
        """
        -- noinspection SqlResolveForStatement
        UPDATE [GIS].[{0}] SET IsDeleted = 1 WHERE Id IN @id;
        """;

    private const string SwitchAliasQuery =
        """
        -- noinspection SqlResolveForStatement
        DROP SYNONYM [GIS].[CLS_Road]; CREATE SYNONYM [GIS].[CLS_Road] FOR [GIS].[{0}];
        """;

    private const string BulkInsertClassifiersQuery =
        """
        -- noinspection SqlResolveForStatement
        INSERT INTO [GIS].[{0}] (Code, DisplayName, CountyCode, ParishCode, Language, IsDeleted) VALUES
        """;

    private const string UpdateClassifierQuery =
        """
        -- noinspection SqlResolveForStatement
        UPDATE [GIS].[{0}] SET DisplayName = @DisplayName, CountyCode = @CountyCode, ParishCode = @ParishCode WHERE Id = @Id;
        """;

    #endregion

    public IEnumerable<RoadClassifier> GetSourceClassifiers() {
        using var connection = this.CreateOpenSourceConnection();
        var records = connection.Query<RoadClassifier>(RoadRepository.FetchSourceDataQuery);

        return records;
    }

    public IEnumerable<RoadClassifier> GetDestinationClassifiers(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        var records = connection.Query<RoadClassifier>(string.Format(RoadRepository.FetchDestinationDataQuery, table));
        
        return records;
    }

    public IEnumerable<RoadClassifier> GetHierarchy()
    {
        using var connection = this.CreateOpenSourceConnection();
        var records = connection.Query<RoadClassifier>(RoadRepository.FetchSourceDataHierarchyQuery);
        
        return records;
    }
    
    public void MarkForDelete(string table, object @params) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(RoadRepository.MarkForDeleteQuery, table), @params);
    }

    public void SwitchAliasTo(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(RoadRepository.SwitchAliasQuery, table));
    }

    public void BulkInsertClassifiers(IReadOnlyCollection<RoadClassifier> classifiers, string table)
    {
        if (!classifiers.Any()) return;

        var query = QueryBuilder.BuildGetUsersQuery(classifiers, string.Format(RoadRepository.BulkInsertClassifiersQuery, table));
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(query);
    }

    public void UpdateClassifier(RoadClassifier? classifier, string table)
    {
        if (classifier is null) return;
        
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(RoadRepository.UpdateClassifierQuery, table), classifier);
    }

    private static class QueryBuilder
    {
        public static string BuildGetUsersQuery(IReadOnlyCollection<RoadClassifier> classifiers, string query)
        {
            var values = string.Join(",", classifiers.Select(data =>
                $"({data.Code.ToNullOrEscape()}, " +
                $"{data.DisplayName.ToNullOrEscape()}, " +
                $"{data.CountyCode.ToNullOrEscape()}, " +
                $"{data.ParishCode.ToNullOrEscape()}, " +
                $"1, " +
                $"0)\n"));
  
            return query + values;
        }
    }
}