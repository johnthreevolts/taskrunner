﻿namespace OperativeServiceImporter.Record;

public record OperativeServiceEmployee(string EmployeeDisplayName)
{
    public OperativeServiceEmployee() : this(string.Empty) { }
}