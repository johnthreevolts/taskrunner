﻿namespace TaskCore;

public class GracefulException : Exception
{
    public readonly bool HasMessage = false;

    public GracefulException() {}

    public GracefulException(string message) : base(message)
    {
        this.HasMessage = true;
    }
}