﻿using OperativeServiceImporter.Record;

namespace OperativeServiceImporter.Class;

public static class Validator
{
    public static bool ValidateTimeOverlap(IEnumerable<Unit> previewUnits, out List<string> errors)
    {
        errors = previewUnits
            .Where(pu => pu.ValidFrom > pu.ValidUntil)
            .Select(pu => $"❌  Time overlap - {pu.Code} {pu.DisplayName} {pu.ValidFrom} - {pu.ValidUntil}.").ToList();
        
        return errors.Any();
    }

    public static bool ValidateDuplicateWithTimeOverlap(IEnumerable<Unit> previewUnits, out List<string> errors)
    {
        var duplicateCodeUnits = previewUnits.GroupBy(pu => pu.Code).Where(dcu => dcu.Count() > 1);
        errors = (from duplicates in duplicateCodeUnits
            from unit in duplicates
            where duplicates.Any(duplicate =>
                (duplicate.ValidFrom >= unit.ValidFrom && duplicate.ValidFrom <= unit.ValidUntil) ||
                (duplicate.ValidUntil >= unit.ValidFrom && duplicate.ValidUntil <= unit.ValidUntil))
            select $"❌  There are units with code - {duplicates.Key}, which time overlap.").ToList();
        
        return errors.Any();
    }

    public static IEnumerable<string> ValidateTree(IReadOnlyCollection<Unit> previewUnits, DateTime from, DateTime to)
    {
        var errors = new List<string>();
        if (!previewUnits.Any()) return errors;

        var rootCount = previewUnits
            .Select(p => p.ParentCode)
            .Count(string.IsNullOrEmpty);
        switch (rootCount)
        {
            case 0:
                errors.Add("🔗 There is no root!");
                break;
            case > 1:
                errors.Add("🔗 There is more then one root!");
                break;
        }

        var selfReferencingNodes = previewUnits
            .Where(u => u.Code == u.ParentCode)
            .ToList();
        if (selfReferencingNodes.Any()) errors.Add($"🔗 There is circular reference, '{string.Join(", ", selfReferencingNodes.Select(srn => $"{srn.Code} {srn.DisplayName}"))}' points on itself.");

        var unitCodes = new HashSet<string>(previewUnits.Select(u => u.Code));
        var unitParentCodes = new HashSet<string>(previewUnits.Where(u => !string.IsNullOrEmpty(u.ParentCode)).Select(u => u.ParentCode));
        var detachedUnitsCodes = unitParentCodes.Except(unitCodes).ToList();
        var detachedUnits = previewUnits
            .Where(up => detachedUnitsCodes.Contains(up.ParentCode))
            .ToList();
        if (detachedUnitsCodes.Any()) errors.Add($"🔗 There are {detachedUnits.Count} detached element. With codes: {string.Join(", ", detachedUnits.Select(u => u.Code).Distinct())}.");

        if (errors.Any())
        {
            errors.Insert(0, $"❌  Operative service tree classifier in date interval from {from.ToLongDateString()} to {to.ToLongDateString()} got issues:");
        }

        return errors;
    }

    public static IEnumerable<string> ValidateUserProfiles(IReadOnlyCollection<Unit> previewUnits, IEnumerable<DetachedProfile> profiles)
    {
        var warnings = new List<string>();
        foreach (var profileGroup in profiles.GroupBy(p => $"{p.FirstName} {p.LastName}"))
        {
            var longestStr = profileGroup.Select(s => s.Role).OrderByDescending(s => s.Length).First().Length;
            warnings.Add($"🔗 {profileGroup.Key} IS NOW DETACHED FROM HIS PROFILES:");
            foreach (var profile in profileGroup)
            {
                warnings.Add($"   - {profile.Role}".PadRight(longestStr + 5) + $" in {profile.DisplayName}");
            }
        }

        return warnings;
    }

    public static IEnumerable<string> ValidateNumberGenerators(IReadOnlyCollection<Unit> previewUnits, IEnumerable<NumberType> numberGenerators)
    {
        var warnings = new List<string>();
        foreach (var numberGenerator in numberGenerators)
        {
            warnings.Add($"   - {numberGenerator.DisplayName}");
        }
        if (warnings.Any())
        {
            warnings.Insert(0, "🔗 List of detached number generators:");
        }

        return warnings;
    }

    public static IEnumerable<string> ValidateEmergencyServiceVehicles(IReadOnlyCollection<Unit> previewUnits, IEnumerable<EmergencyServiceVehicle> emergencyServiceVehicles)
    {
        var warnings = new List<string>();
        foreach (var vehicle in emergencyServiceVehicles)
        {
            warnings.Add($"   - {vehicle.DisplayName}");
        }
        if (warnings.Any())
        {
            warnings.Insert(0, "🔗 List of detached emergency service vehicles:");
        }

        return warnings;
    }

    public static IEnumerable<string> ValidateCrews(IReadOnlyCollection<Unit> previewUnits, IEnumerable<Crew> crews)
    {
        var warnings = new List<string>();
        foreach (var crew in crews)
        {
            warnings.Add($"   - {crew.Name}");
        }
        if (warnings.Any())
        {
            warnings.Insert(0, "🔗 List of detached crews:");
        }

        return warnings;
    }

    public static IEnumerable<string> ValidateOperativeServiceEmployees(IReadOnlyCollection<Unit> previewUnits, IEnumerable<OperativeServiceEmployee> operativeServiceEmployees)
    {
        var warnings = new List<string>();
        foreach (var employee in operativeServiceEmployees)
        {
            warnings.Add($"   - {employee.EmployeeDisplayName}");
        }
        if (warnings.Any())
        {
            warnings.Insert(0, "🔗 List of detached operative service employee:");
        }

        return warnings;
    }
}