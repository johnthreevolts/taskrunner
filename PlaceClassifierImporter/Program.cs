using TaskCore;
using System.CommandLine;
using PlaceClassifierImporter.Class;
using PlaceClassifierImporter.Entity;

namespace PlaceClassifierImporter;

public class Payload : Runnable
{
    public override string PayloadName => "PlaceClassifierImporter";
    private const int CHUNK_SIZE = 1000;

    public override Command GetCommands(Option moduleOption)
    {
        var sourceOption = Helper.CreateOption<string>(
            "--source",
            "-s",
            "TR_PostgreSQL_ConnectionString",
            "Connection string of source DB");
        var destinationOption = Helper.CreateOption<string>(
            "--destination",
            "-d",
            "TR_MS_SQL_ConnectionString",
            "Connection string of destination DB");

        var rootCommand = new RootCommand { moduleOption, sourceOption, destinationOption };

        rootCommand.SetHandler(
            Helper.RunOrLog<string, string>(this.Run),
            sourceOption,
            destinationOption);

        return rootCommand;
    }

    private void Run(string sourceDbConnection, string destinationDbConnection)
    {
        this
            .Handle(new CountyClassifier(), new CountyRepository(sourceDbConnection, destinationDbConnection))
            .Handle(new CityClassifier(), new CityRepository(sourceDbConnection, destinationDbConnection))
            .Handle(new ParishClassifier(), new ParishRepository(sourceDbConnection, destinationDbConnection))
            .Handle(new RoadClassifier(), new RoadRepository(sourceDbConnection, destinationDbConnection))
            .Handle(new VillageClassifier(), new VillageRepository(sourceDbConnection, destinationDbConnection))
            .Handle(new StreetClassifier(), new StreetRepository(sourceDbConnection, destinationDbConnection))
            .Handle(new BuildingClassifier(), new BuildingRepository(sourceDbConnection, destinationDbConnection))
            ;
    }

    private Payload Handle<TClassifier, TRepository>(TClassifier classifier, TRepository repository)
        where TClassifier : Classifier<TClassifier>
        where TRepository :  IClassifierRepository<TClassifier>
    {
        var name = classifier.GetType().Name;
        var versionUpdateRequired = false;
        Console.WriteLine($"====================Start of {name}".PadRight(70, '='));

        var source = this.FetchDataFromSource<TClassifier, TRepository>(repository);
        var destination = this.FetchDataFromDestination<TClassifier, TRepository>(repository, classifier.FullTableName);

        versionUpdateRequired |= this.DeleteObsolete(destination, source, repository, classifier.FullTableName);
        versionUpdateRequired |= this.InsertNew(source, destination, repository, classifier.FullTableName);
        versionUpdateRequired |= this.UpdateExisting(destination, source, classifier.Comparer(), repository, classifier.FullTableName);

        repository.SwitchAliasTo(classifier.FullTableName);
        classifier.TableNamePostfix = "A";
        
        destination = null;
        GC.Collect();
        destination = this.FetchDataFromDestination<TClassifier, TRepository>(repository, classifier.FullTableName);

        versionUpdateRequired |= this.DeleteObsolete(destination, source, repository, classifier.FullTableName);
        versionUpdateRequired |= this.InsertNew(source, destination, repository, classifier.FullTableName);
        versionUpdateRequired |= this.UpdateExisting(destination, source, classifier.Comparer(), repository, classifier.FullTableName);

        repository.SwitchAliasTo(classifier.FullTableName);
        if (versionUpdateRequired)
        {
            repository.UpdateVersioning(classifier.TableName);
        }

        Console.WriteLine($"====================End of {name}".PadRight(70, '='));

        return this;
    }

    private bool InsertNew<TClassifier, TRepository>(Dictionary<string, TClassifier> source, Dictionary<string, TClassifier> destination, TRepository repository, string table)
        where TClassifier : Classifier<TClassifier>
        where TRepository : IClassifierRepository<TClassifier>
    {
        var forInserting = source.Keys.Except(destination.Keys).ToList();
        Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Found {forInserting.Count} keys for inserting.");
        var chunkNumber = 0;
        foreach (var chunk in forInserting.Chunk(Payload.CHUNK_SIZE))
        {
            Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Start of chunk #{++chunkNumber}.");
            repository.BulkInsertClassifiers(chunk.Select(k => source[k]).ToList(), table);
            if (chunkNumber % 20 == 0)
            {
                GC.Collect();
            }
        }

        return forInserting.Count > 0;
    }

    private bool UpdateExisting<TClassifier, TRepository>(
        Dictionary<string, TClassifier> destination,
        Dictionary<string, TClassifier> source,
        IEqualityComparer<TClassifier> comparer,
        TRepository repository,
        string table)
        where TClassifier : Classifier<TClassifier>
        where TRepository : IClassifierRepository<TClassifier>
    {
        var updateableClassifiers = new List<TClassifier>();
        var forUpdatingKeys = destination.Keys.Intersect(source.Keys);
        foreach (var code in forUpdatingKeys)
        {
            if (comparer.Equals(destination[code], source[code])) continue;
            var record = source[code];
            record.Id = destination[code].Id;
            updateableClassifiers.Add(record);
        }

        Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Found {updateableClassifiers.Count} keys for updating.");
        var chunkNumber = 0;
        foreach (var chunk in updateableClassifiers.Chunk(Payload.CHUNK_SIZE))
        {
            Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Start of chunk #{++chunkNumber}.");
            foreach (var piece in chunk)
            {
                repository.UpdateClassifier(piece, table);
            }
            if (chunkNumber % 20 == 0)
            {
                GC.Collect();
            }
        }

        return updateableClassifiers.Count > 0;
    }

    private bool DeleteObsolete<TClassifier, TRepository>(Dictionary<string, TClassifier> destination, Dictionary<string, TClassifier> source, TRepository repository, string table)
        where TClassifier : Classifier<TClassifier>
        where TRepository : IClassifierRepository<TClassifier>
    {
        var forDeleting = destination.Keys.Except(source.Keys).ToList();
        Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Found {forDeleting.Count} keys for deleting.");

        var chunkNumber = 0;
        foreach (var chunk in forDeleting.Chunk(Payload.CHUNK_SIZE))
        {
            Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Start of chunk #{++chunkNumber}.");
            repository.MarkForDelete(table, new { id = chunk.Select(k => destination[k]).Select(c => c.Id) });
        }

        return forDeleting.Count > 0;
    }

    private Dictionary<string, TClassifier> FetchDataFromSource<TClassifier, TRepository>(TRepository repository)
        where TClassifier : Classifier<TClassifier>
        where TRepository : IClassifierRepository<TClassifier>
    {
        var source = repository.GetSourceClassifiers().ToList();
        var hierarchy = repository.GetHierarchy().ToDictionary(i => i.Code);
        Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Read {source.Count} records from source.");  
        if (hierarchy.Any())
        {
            source = source.Select(s => s.Enrich(s, hierarchy)).ToList();
            Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Enriched {source.Count} records from hierarchy.");
        }

        return source.ToDictionary(k => k.Code);
    }

    private Dictionary<string, TClassifier> FetchDataFromDestination<TClassifier, TRepository>(TRepository repository, string table)
        where TClassifier : Classifier<TClassifier>
        where TRepository : IClassifierRepository<TClassifier>
    {
        var destination = repository.GetDestinationClassifiers(table).ToDictionary(k => k.Code);
        Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Read {destination.Count} records from destination.");

        return destination;
    }
}