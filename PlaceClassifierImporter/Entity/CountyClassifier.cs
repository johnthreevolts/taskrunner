﻿namespace PlaceClassifierImporter.Entity;

public record CountyClassifier : Classifier<CountyClassifier>
{
    public override string TableName => "CLS_County";
    public override IEqualityComparer<CountyClassifier> Comparer() => new EqualityComparer();
    
    private sealed class EqualityComparer : IEqualityComparer<CountyClassifier>
    {
        public bool Equals(CountyClassifier? x, CountyClassifier? y)
        {
            if (object.ReferenceEquals(x, y)) return true;
            if (object.ReferenceEquals(x, null)) return false;
            if (object.ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Code == y.Code &&
                   x.DisplayName == y.DisplayName;
        }

        public int GetHashCode(CountyClassifier obj)
        {
            return HashCode.Combine(obj.Code, obj.DisplayName);
        }
    }
}