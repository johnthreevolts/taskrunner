﻿namespace PatrollingRouteImporter.Record;

public record PatrollingRoute(long? Id, long RouteId, string Name, DateTime? DeletedAt)
{
    public PatrollingRoute() : this(default, default, string.Empty, null) { }

    public PatrollingRoute(long RouteId, string Name, DateTime DeletedAt) : this(null, RouteId, Name, DeletedAt) { }

    public virtual bool Equals(PatrollingRoute? other)
    {
        if (object.ReferenceEquals(this, other))
            return true;
        
        if (other is null)
            return false;
        
        return this.RouteId == other.RouteId &&
               this.Name == other.Name &&
               this.DeletedAt == other.DeletedAt;
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = this.RouteId.GetHashCode();
            hashCode = (hashCode * 397) ^ (this.Name != null ? this.Name.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (this.DeletedAt != null ? this.DeletedAt.GetHashCode() : 0);
            return hashCode;
        }
    }
}
