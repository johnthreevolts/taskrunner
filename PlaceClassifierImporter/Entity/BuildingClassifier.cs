﻿namespace PlaceClassifierImporter.Entity;

public record BuildingClassifier : Classifier<BuildingClassifier>
{
    public override string TableName => "CLS_Building";
    public string? CityCode { get; set; }
    public string? ParishCode { get; set; }
    public string? StreetCode { get; set; }
    public string? VillageCode { get; set; }
    public string? CountyCode { get; set; }

    public override BuildingClassifier Enrich(BuildingClassifier classifier, Dictionary<string, BuildingClassifier> source)
    {
        if (classifier.CityCode is null && classifier.StreetCode is not null)
        {
            classifier.CityCode = source[classifier.StreetCode]?.CityCode ?? null;
        }
        if (classifier.VillageCode is null && classifier.StreetCode is not null)
        {
            classifier.VillageCode = source[classifier.StreetCode]?.VillageCode ?? null;
        }
        if (classifier.ParishCode is null && classifier.VillageCode is not null)
        {
            classifier.ParishCode = source[classifier.VillageCode]?.ParishCode ?? null;
        }
        if (classifier.CountyCode is null && classifier.CityCode is not null)
        {
            if (classifier.CityCode is not null && source.TryGetValue(classifier.CityCode, out var cityClassifier))
            {
                classifier.CountyCode = cityClassifier.CountyCode;
            }
            else if (classifier.ParishCode is not null &&source.TryGetValue(classifier.ParishCode, out var parishCodeClassifier))
            {
                classifier.CountyCode = parishCodeClassifier.CountyCode;
            }
        }
        
        return classifier;
    }

    public override IEqualityComparer<BuildingClassifier> Comparer() => new EqualityComparer();
    
    private sealed class EqualityComparer : IEqualityComparer<BuildingClassifier>
    {
        public bool Equals(BuildingClassifier? x, BuildingClassifier? y)
        {
            if (object.ReferenceEquals(x, y)) return true;
            if (object.ReferenceEquals(x, null)) return false;
            if (object.ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Code == y.Code &&
                   x.DisplayName == y.DisplayName &&
                   x.CityCode == y.CityCode &&
                   x.ParishCode == y.ParishCode &&
                   x.StreetCode == y.StreetCode &&
                   x.VillageCode == y.VillageCode &&
                   x.CountyCode == y.CountyCode;
        }
        
        public int GetHashCode(BuildingClassifier obj)
        {
            return HashCode.Combine(obj.Code, obj.DisplayName, obj.CityCode, obj.ParishCode, obj.StreetCode, obj.VillageCode, obj.CountyCode);
        }
    }
}