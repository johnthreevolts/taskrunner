﻿using Dapper;
using PlaceClassifierImporter.Entity;
using TaskCore;

namespace PlaceClassifierImporter.Class;

public class CountyRepository : Repository, IClassifierRepository<CountyClassifier>
{
    public CountyRepository(string sourceConnectionString, string destinationConnectionString): base(sourceConnectionString, destinationConnectionString) {}

    #region Postgres fetch queries

    private const string FetchSourceDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT CAST(kods as text) AS Code,
               nosaukums AS DisplayName
        FROM varis.iedalijums
        WHERE tips_cd = 113
          AND statuss = 'EKS'
        """;

    #endregion

    #region MSSQL fetch queries

    private const string FetchDestinationDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT Id, Code, DisplayName FROM [GIS].[{0}] WITH (NOLOCK) WHERE IsDeleted = 0;
        """;

    #endregion

    #region Update queries

    private const string MarkForDeleteQuery =
        """
        -- noinspection SqlResolveForStatement
        UPDATE [GIS].[{0}] SET IsDeleted = 1 WHERE Id IN @id;
        """;

    private const string SwitchAliasQuery =
        """
        -- noinspection SqlResolveForStatement
        DROP SYNONYM [GIS].[CLS_County]; CREATE SYNONYM [GIS].[CLS_County] FOR [GIS].[{0}];
        """;

    private const string BulkInsertClassifiersQuery =
        """
        -- noinspection SqlResolveForStatement
        INSERT INTO [GIS].[{0}] (Code, DisplayName, Language, IsDeleted) VALUES
        """;

    private const string UpdateClassifierQuery =
        """
        -- noinspection SqlResolveForStatement
        UPDATE [GIS].[{0}] SET DisplayName = @DisplayName WHERE Id = @Id;
        """;

    #endregion

    public IEnumerable<CountyClassifier> GetSourceClassifiers() {
        using var connection = this.CreateOpenSourceConnection();
        var records = connection.Query<CountyClassifier>(CountyRepository.FetchSourceDataQuery);

        return records;
    }

    public IEnumerable<CountyClassifier> GetDestinationClassifiers(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        var records = connection.Query<CountyClassifier>(string.Format(CountyRepository.FetchDestinationDataQuery, table));
        
        return records;
    }

    public IEnumerable<CountyClassifier> GetHierarchy()
    {
        return new List<CountyClassifier>();
    }
    
    public void MarkForDelete(string table, object @params) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(CountyRepository.MarkForDeleteQuery, table), @params);
    }

    public void SwitchAliasTo(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(CountyRepository.SwitchAliasQuery, table));
    }

    public void BulkInsertClassifiers(IReadOnlyCollection<CountyClassifier> classifiers, string table)
    {
        if (!classifiers.Any()) return;

        var query = QueryBuilder.BuildGetUsersQuery(classifiers, string.Format(CountyRepository.BulkInsertClassifiersQuery, table));
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(query);
    }

    public void UpdateClassifier(CountyClassifier? classifier, string table)
    {
        if (classifier is null) return;
        
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(CountyRepository.UpdateClassifierQuery, table), classifier);
    }

    private static class QueryBuilder
    {
        public static string BuildGetUsersQuery(IReadOnlyCollection<CountyClassifier> classifiers, string query)
        {
            var values = string.Join(",", classifiers.Select(data =>
                $"({data.Code.ToNullOrEscape()}, " +
                $"{data.DisplayName.ToNullOrEscape()}, " +
                $"1, " +
                $"0)\n"));
  
            return query + values;
        }
    }
}
