﻿namespace PlaceClassifierImporter.Entity;

public record Classifier<TClassifier>
{
    public int Id { get; set; }
    public virtual string TableName { get; set; } = string.Empty;
    public string TableNamePostfix { get; set; } = "B";
    public string FullTableName => this.TableName + this.TableNamePostfix;
    public string Code { get; set; }
    public string DisplayName { get; set; }

    public virtual TClassifier Enrich(TClassifier classifier, Dictionary<string, TClassifier> source) => classifier;

    public virtual IEqualityComparer<TClassifier> Comparer() => default!;
}