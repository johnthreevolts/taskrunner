using System.CommandLine;

namespace TaskCore;

public abstract class Runnable
{
    public abstract string PayloadName { get; }

    public abstract Command GetCommands(Option moduleOption);
}