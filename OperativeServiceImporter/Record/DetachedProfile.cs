﻿namespace OperativeServiceImporter.Record;

public record DetachedProfile(
    string FirstName,
    string LastName,
    string Role,
    string DisplayName
)
{
    public DetachedProfile() : this(
        string.Empty,
        string.Empty,
        string.Empty,
        string.Empty) { }
}