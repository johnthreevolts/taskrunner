﻿namespace PlaceClassifierImporter.Entity;

public record VillageClassifier : Classifier<VillageClassifier>
{
    public override string TableName => "CLS_Village";
    public string? CountyCode { get; set; }
    public string? ParishCode { get; set; }

    public override IEqualityComparer<VillageClassifier> Comparer() => new EqualityComparer();
    
    private sealed class EqualityComparer : IEqualityComparer<VillageClassifier>
    {
        public bool Equals(VillageClassifier? x, VillageClassifier? y)
        {
            if (object.ReferenceEquals(x, y)) return true;
            if (object.ReferenceEquals(x, null)) return false;
            if (object.ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Code == y.Code &&
                   x.DisplayName == y.DisplayName &&
                   x.CountyCode == y.CountyCode &&
                   x.ParishCode == y.ParishCode;
        }

        public int GetHashCode(VillageClassifier obj)
        {
            return HashCode.Combine(obj.Code, obj.DisplayName, obj.CountyCode, obj.ParishCode);
        }
    }
}