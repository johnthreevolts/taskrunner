﻿namespace PlaceClassifierImporter.Entity;

public record RoadClassifier : Classifier<RoadClassifier>
{
    public override string TableName => "CLS_Road";
    public string? CountyCode { get; set; }
    public string? ParishCode { get; set; }

    public override RoadClassifier Enrich(RoadClassifier classifier, Dictionary<string, RoadClassifier> source)
    {
        if (classifier.CountyCode is null && classifier.ParishCode is not null)
        {
            classifier.CountyCode = source[classifier.ParishCode]?.CountyCode ?? null;
        }
        
        return classifier;
    }
    
    public override IEqualityComparer<RoadClassifier> Comparer() => new EqualityComparer();
    
    private sealed class EqualityComparer : IEqualityComparer<RoadClassifier>
    {
        public bool Equals(RoadClassifier? x, RoadClassifier? y)
        {
            if (object.ReferenceEquals(x, y)) return true;
            if (object.ReferenceEquals(x, null)) return false;
            if (object.ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Code == y.Code &&
                   x.DisplayName == y.DisplayName &&
                   x.CountyCode == y.CountyCode &&
                   x.ParishCode == y.ParishCode;
        }

        public int GetHashCode(RoadClassifier obj)
        {
            return HashCode.Combine(obj.Code, obj.DisplayName, obj.CountyCode, obj.ParishCode);
        }
    }
}