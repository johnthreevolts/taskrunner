﻿namespace OperativeServiceImporter.Record;

public record NumberType(string DisplayName)
{
    public NumberType() : this(string.Empty) { }
}