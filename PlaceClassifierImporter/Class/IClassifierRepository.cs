﻿namespace PlaceClassifierImporter.Class;

public interface IClassifierRepository<TClassifier>
{
    IEnumerable<TClassifier> GetSourceClassifiers();
    IEnumerable<TClassifier> GetDestinationClassifiers(string table);
    IEnumerable<TClassifier> GetHierarchy();
    void MarkForDelete(string table, object @params);
    void SwitchAliasTo(string table);
    void BulkInsertClassifiers(IReadOnlyCollection<TClassifier> classifiers, string table);
    void UpdateClassifier(TClassifier? classifier, string table);
    void UpdateVersioning(string table);
}