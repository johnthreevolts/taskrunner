﻿using System.Data;
using System.Data.SqlClient;
using Dapper;
using OperativeServiceImporter.Record;

namespace OperativeServiceImporter.Class;

public class Repository
{
    private readonly string _connectionString;
    
    public Repository(string connectionString)
    {
        this._connectionString = connectionString;
    }

    #region Fetch queries

    private const string FetchTableSettingQuery =
        """
        SELECT TOP 1 r.Value FROM (
            SELECT s.Value, 1 AS OrderValue FROM [mfw].[Setting] AS s WHERE s.Type = '72'
            UNION
            SELECT sdv.DefaultValue AS Value, 2 AS OrderValue FROM [mfw].[SettingDefaultValue] AS sdv WHERE sdv.SettingType = '72'
        ) AS r
        ORDER BY OrderValue;
        """;

    private const string FetchOperatingServiceQuery =
        "SELECT TOP 1 Code FROM [mfw].[CLS_VekOperationalService] WHERE Code = @Code;";

    private const string FetchPreviewUnitQuery =
        """
         SELECT
         Id, UnitCode AS Code, DisplayName, Language, IsDeleted, InstitutionCode, InstitutionDisplayName, Fullname, Address, EMail, CellNumber, FaxNumber, Hierarchy, HasLocation, UrCode, VissCode, ValidFrom, ValidUntil, ParentCode, NULL AS EffectiveEndDate
         FROM [mfw].[CLS_UnitPreview]
         WHERE InstitutionCode = @OperatingService AND IsDeleted = 0;
         """;

    private const string FetchUnitQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT
        Id, Code, DisplayName, Language, 0 AS IsDeleted, InstitutionCode, InstitutionDisplayName, Fullname, Address, EMail, CellNumber, FaxNumber, Hierarchy, HasLocation, UrCode, VissCode, ValidFrom, ValidUntil, ParentCode, EffectiveEndDate
        FROM [mfw].[{0}]
        WHERE InstitutionCode = @OperatingService;
        """;

    #endregion

    #region Update queries
    
    private const string TruncateAuthorityTablePairQuery =
        """
        TRUNCATE TABLE [mfw].[{0}];
        TRUNCATE TABLE [mfw].[{1}];
        """;
    
    private const string CopyToInactiveAuthorityHistoryTableQuery =
        """
        -- noinspection SqlResolveForStatement
        INSERT INTO [mfw].[{0}] ([Code], [DisplayName], [Language], [InstitutionCode], [InstitutionDisplayName], [Fullname], [Address], [EMail], [CellNumber], [FaxNumber], [Hierarchy], [HasLocation], [UrCode], [VissCode], [ValidFrom], [ValidUntil], [ParentCode], [EffectiveEndDate])
        SELECT [Code], [DisplayName], [Language], [InstitutionCode], [InstitutionDisplayName], [Fullname], [Address], [EMail], [CellNumber], [FaxNumber], [Hierarchy], [HasLocation], [UrCode], [VissCode], [ValidFrom], [ValidUntil], [ParentCode], [EffectiveEndDate]
        FROM [mfw].[{1}];
        """;
    
    private const string CopyOtherServicesToInactiveAuthorityTableQuery =
        """
        -- noinspection SqlResolveForStatement
        INSERT INTO [mfw].[{0}] ([Id], [Code], [DisplayName], [Language], [InstitutionCode], [InstitutionDisplayName], [Fullname], [Address], [EMail], [CellNumber], [FaxNumber], [Hierarchy], [HasLocation], [UrCode], [VissCode], [ValidFrom], [ValidUntil], [ParentCode], [EffectiveEndDate])
        SELECT [Id], [Code], [DisplayName], [Language], [InstitutionCode], [InstitutionDisplayName], [Fullname], [Address], [EMail], [CellNumber], [FaxNumber], [Hierarchy], [HasLocation], [UrCode], [VissCode], [ValidFrom], [ValidUntil], [ParentCode], [EffectiveEndDate]
        FROM [mfw].[{1}]
        WHERE InstitutionCode != @OperatingService;
        """;

    private const string UpdateUnitPreviewImportDateQuery =
        """
        -- noinspection SqlWithoutWhere
        UPDATE [mfw].[CLS_UnitPreview] SET ImportDate = GETUTCDATE()
        WHERE InstitutionCode = @OperatingService;
        """;
    
    private const string SwitchActualTablePairQuery =
        """
         MERGE INTO [mfw].[Setting] AS Target
         USING (SELECT '72' AS Type, @ActualTable AS Value) AS Source
         ON (Target.Type = Source.Type)
         WHEN MATCHED THEN
             UPDATE SET Target.Value = Source.Value
         WHEN NOT MATCHED THEN
             INSERT (Type, Value)
             VALUES (Source.Type, Source.Value);
         """;

    private const string InsertIntoInactiveAuthorityTableQuery =
        """
        -- noinspection SqlResolveForStatement
        INSERT INTO [mfw].[{0}] (Id, Code, DisplayName, Language, InstitutionCode, InstitutionDisplayName, Fullname, Address, EMail, CellNumber, FaxNumber, Hierarchy, HasLocation, UrCode, VissCode, ValidFrom, ValidUntil, ParentCode, EffectiveEndDate)
         VALUES (@Id, @Code, @DisplayName, @Language, @InstitutionCode, @InstitutionDisplayName, @Fullname, @Address, @EMail, @CellNumber, @FaxNumber, @Hierarchy, @HasLocation, @UrCode, @VissCode, @ValidFrom, @ValidUntil, @ParentCode, @EffectiveEndDate);
        """;

    private const string InsertIntoInactiveAuthorityHistoryTableQuery =
        """
        -- noinspection SqlResolveForStatement
        INSERT INTO [mfw].[{0}] (Code, DisplayName, Language, InstitutionCode, InstitutionDisplayName, Fullname, Address, EMail, CellNumber, FaxNumber, Hierarchy, HasLocation, UrCode, VissCode, ValidFrom, ValidUntil, ParentCode, EffectiveEndDate)
         VALUES (@Code, @DisplayName, @Language, @InstitutionCode, @InstitutionDisplayName, @Fullname, @Address, @EMail, @CellNumber, @FaxNumber, @Hierarchy, @HasLocation, @UrCode, @VissCode, @ValidFrom, @ValidUntil, @ParentCode, @EffectiveEndDate);
        """;
    
    #endregion

    public int? GetTableSetting()
    {
        using var connection = this.CreateOpenConnection();
        var setting = connection.QueryFirstOrDefault<string>(Repository.FetchTableSettingQuery);
        if (int.TryParse(setting, out var result) && result is 1 or 2 )
        {
            return result;
        }

        return null;
    }
    
    public string? GetOperatingService(string operatingService)
    {
        using var connection = this.CreateOpenConnection();
        var operatingServiceCode = connection.QueryFirstOrDefault<string>(Repository.FetchOperatingServiceQuery, new { Code = operatingService });
      
        return operatingServiceCode is [_, ..] ? operatingServiceCode : null;
    }
    
    public IEnumerable<Unit> GetPreviewUnits(string operatingService)
    {
        using var connection = this.CreateOpenConnection();
        var records = connection.Query<Unit>(Repository.FetchPreviewUnitQuery, new { OperatingService = operatingService });
    
        return records;
    }

    public IEnumerable<Unit> GetUnits(string authorityTable, string operatingService)
    {
        using var connection = this.CreateOpenConnection();
        var records = connection.Query<Unit>(
            string.Format(Repository.FetchUnitQuery, authorityTable),
            new
            {
                OperatingService = operatingService
            });

        return records;
    }

    public void TruncateAuthorityTablePair(string inactiveAuthorityTable, string inactiveAuthorityHistoryTable)
    {
        using var connection = this.CreateOpenConnection();
        connection.Execute(string.Format(
            Repository.TruncateAuthorityTablePairQuery,
            inactiveAuthorityTable,
            inactiveAuthorityHistoryTable)
        );
    }

    public void CopyToInactiveAuthorityHistoryTable(string authorityHistoryTable, string inactiveAuthorityHistoryTable)
    {
        using var connection = this.CreateOpenConnection();
        connection.Execute(string.Format(
            Repository.CopyToInactiveAuthorityHistoryTableQuery,
            inactiveAuthorityHistoryTable,
            authorityHistoryTable)
        );
    }

    public void CopyOtherServicesToInactiveAuthorityTable(string authorityTable, string inactiveAuthorityTable, string operatingService)
    {
        using var connection = this.CreateOpenConnection();
        connection.Execute(string.Format(
            Repository.CopyOtherServicesToInactiveAuthorityTableQuery,
            inactiveAuthorityTable,
            authorityTable),
            new {
                OperatingService = operatingService
            }
        );
    }

    public void UpdateUnitPreviewImportDate(string operatingService)
    {
        using var connection = this.CreateOpenConnection();
        connection.Execute(Repository.UpdateUnitPreviewImportDateQuery, new
        {
            OperatingService = operatingService
        });
    }

    public void SwitchActualTablePair(int actualTable)
    {
        using var connection = this.CreateOpenConnection();
        connection.Execute(Repository.SwitchActualTablePairQuery, new { ActualTable = actualTable });
    }
    
    public void InsertIntoInactiveAuthority(IReadOnlyCollection<Unit> units, string inactiveAuthorityTable)
    {
        if (!units.Any()) return;
        
        using var connection = this.CreateOpenConnection();
        connection.Execute(
            string.Format(Repository.InsertIntoInactiveAuthorityTableQuery, inactiveAuthorityTable),
            units
        );
    }
    
    public void InsertIntoInactiveAuthorityHistory(IReadOnlyCollection<Unit> units, string inactiveAuthorityHistoryTable)
    {
        if (!units.Any()) return;
        
        using var connection = this.CreateOpenConnection();
        connection.Execute(
            string.Format(Repository.InsertIntoInactiveAuthorityHistoryTableQuery, inactiveAuthorityHistoryTable),
            units
        );
    }
    
    public IEnumerable<DetachedProfile> GetDetachedUserProfiles()
    {
        using var connection = this.CreateOpenConnection();
        var records = connection.Query<DetachedProfile>("SELECT * FROM mfw.CLS_Unit WHERE 1 = 2");
    
        return records;
    }
    
    public IEnumerable<NumberType> GetDetachedNumberGenerators()
    {
        using var connection = this.CreateOpenConnection();
        var records = connection.Query<NumberType>("SELECT * FROM mfw.CLS_Unit WHERE 1 = 2");
    
        return records;
    }
    
    public IEnumerable<EmergencyServiceVehicle> GetDetachedEmergencyServiceVehicles()
    {
        using var connection = this.CreateOpenConnection();
        var records = connection.Query<EmergencyServiceVehicle>("SELECT * FROM mfw.CLS_Unit WHERE 1 = 2");
    
        return records;
    }
    
    public IEnumerable<Crew> GetDetachedCrews()
    {
        using var connection = this.CreateOpenConnection();
        var records = connection.Query<Crew>("SELECT * FROM mfw.CLS_Unit WHERE 1 = 2");
    
        return records;
    }
    
    public IEnumerable<OperativeServiceEmployee> GetDetachedOperativeServiceEmployees()
    {
        using var connection = this.CreateOpenConnection();
        var records = connection.Query<OperativeServiceEmployee>("SELECT * FROM mfw.CLS_Unit WHERE 1 = 2");
    
        return records;
    }

    private IDbConnection CreateOpenConnection()
    {
        var connection = new SqlConnection(this._connectionString);
        connection.Open();
        return connection;
    }
}