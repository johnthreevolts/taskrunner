﻿namespace PlaceClassifierImporter.Entity;

public record CityClassifier : Classifier<CityClassifier>
{
    public override string TableName => "CLS_City";
    public string? CountyCode { get; set; }
    public bool IsGeneralCity { get; set; }

    public override IEqualityComparer<CityClassifier> Comparer() => new EqualityComparer();
    
    private sealed class EqualityComparer : IEqualityComparer<CityClassifier>
    {
        public bool Equals(CityClassifier? x, CityClassifier? y)
        {
            if (object.ReferenceEquals(x, y)) return true;
            if (object.ReferenceEquals(x, null)) return false;
            if (object.ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Code == y.Code &&
                   x.DisplayName == y.DisplayName;
        }

        public int GetHashCode(CityClassifier obj)
        {
            return HashCode.Combine(obj.Code, obj.DisplayName);
        }
    }
}