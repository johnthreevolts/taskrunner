﻿using System.CommandLine;
using PatrollingRouteImporter.Class;
using PatrollingRouteImporter.Record;
using TaskCore;

namespace PatrollingRouteImporter;

public class Payload : Runnable
{
    public override string PayloadName => "PatrollingRouteImporter";
    private const int CHUNK_SIZE = 1000;

    public override Command GetCommands(Option moduleOption)
    {
        var sourceOption = Helper.CreateOption<string>(
            "--source",
            "-s",
            "TR_PostgreSQL_ConnectionString",
            "Connection string of source DB");
        var destinationOption = Helper.CreateOption<string>(
            "--destination",
            "-d",
            "TR_MS_SQL_ConnectionString",
            "Connection string of destination DB");

        var rootCommand = new RootCommand { moduleOption, sourceOption, destinationOption };

        rootCommand.SetHandler(
            Helper.RunOrLog<string, string>(this.Run),
            sourceOption,
            destinationOption);

        return rootCommand;
    }

    private void Run(string sourceDbConnection, string destinationDbConnection)
    {
        var fullTableName = "PatrollingRouteA";
        var repository = new Repository(sourceDbConnection, destinationDbConnection);
        var source = this.FetchDataFromSource(repository);
        var destination = this.FetchDataFromDestination(repository, fullTableName);

        this.DeleteObsolete(destination, source, repository,fullTableName);
        this.InsertNew(source, destination, repository, fullTableName);
        this.UpdateExisting(destination, source, repository, fullTableName);

        repository.SwitchAliasTo(fullTableName);
        fullTableName = "PatrollingRouteB";
        
        destination = null;
        GC.Collect();
        destination = this.FetchDataFromDestination(repository, fullTableName);

        this.DeleteObsolete(destination, source, repository, fullTableName);
        this.InsertNew(source, destination, repository, fullTableName);
        this.UpdateExisting(destination, source, repository, fullTableName);

        repository.SwitchAliasTo(fullTableName);
    }

    private void InsertNew(Dictionary<long, PatrollingRoute> source, Dictionary<long, PatrollingRoute> destination, Repository repository, string table)
    {
        var forInserting = source.Keys.Except(destination.Keys).ToList();
        Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Found {forInserting.Count} keys for inserting.");
        var chunkNumber = 0;
        foreach (var chunk in forInserting.Chunk(Payload.CHUNK_SIZE))
        {
            Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $" Start of chunk #{++chunkNumber}.");
            repository.BulkInsertClassifiers(chunk.Select(k => source[k]).ToList(), table);
            if (chunkNumber % 20 == 0)
            {
                GC.Collect();
            }
        }
    }

    private void UpdateExisting(
        Dictionary<long, PatrollingRoute> destination,
        Dictionary<long, PatrollingRoute> source,
        Repository repository,
        string table)
    {
        var forUpdatingKeys = destination.Keys.Intersect(source.Keys);
        var updateableClassifiers = (from chunk in forUpdatingKeys.Chunk(Payload.CHUNK_SIZE)
            from code in chunk
            where destination[code] != source[code]
            select source[code] with { Id = destination[code].Id }).ToList();

        Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Found {updateableClassifiers.Count} keys for updating.");
        var chunkNumber = 0;
        foreach (var chunk in updateableClassifiers.Chunk(Payload.CHUNK_SIZE))
        {
            Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Start of chunk #{++chunkNumber}.");
            foreach (var piece in chunk)
            {
                repository.UpdateClassifier(piece, table);
            }
            if (chunkNumber % 20 == 0)
            {
                GC.Collect();
            }
        }
    }

    private void DeleteObsolete(Dictionary<long, PatrollingRoute> destination, Dictionary<long, PatrollingRoute> source, Repository repository, string table)
    {
        var forDeleting = destination.Keys.Except(source.Keys).ToList();
        Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Found {forDeleting.Count} keys for deleting.");

        var chunkNumber = 0;
        foreach (var chunk in forDeleting.Chunk(Payload.CHUNK_SIZE))
        {
            Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $" Start of chunk #{++chunkNumber}.");
            repository.Delete(table, new { id = chunk.Select(k => destination[k]).Select(c => c.Id) });
        }
    }

    private Dictionary<long, PatrollingRoute> FetchDataFromSource(Repository repository)
    {
        var source = repository.GetSourceClassifiers().ToList();
        Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Read {source.Count} records from source.");

        return source.ToDictionary(k => k.RouteId);
    }

    private Dictionary<long, PatrollingRoute> FetchDataFromDestination(Repository repository, string table)
    {
        var destination = repository.GetDestinationClassifiers(table).ToDictionary(k => k.RouteId);
        Console.WriteLine("🕑 " + DateTime.Now.ToString("HH:mm:ss") + $": Read {destination.Count} records from destination.");

        return destination;
    }
}
