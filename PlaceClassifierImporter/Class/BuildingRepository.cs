﻿using Dapper;
using PlaceClassifierImporter.Entity;
using TaskCore;

namespace PlaceClassifierImporter.Class;

public class BuildingRepository : Repository, IClassifierRepository<BuildingClassifier>
{
    public BuildingRepository(string sourceConnectionString, string destinationConnectionString): base(sourceConnectionString, destinationConnectionString) {}

    #region Postgres fetch queries

    private const string FetchSourceDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT
            vzd_id                                                AS Code,
            addr_name                                             AS DisplayName,
            CASE WHEN next_level = 104 THEN next_level_vzd_id END AS CityCode,
            CASE WHEN next_level = 106 THEN next_level_vzd_id END AS VillageCode,
            CASE WHEN next_level = 107 THEN next_level_vzd_id END AS StreetCode
        FROM varis.vzd_addresses
        WHERE addr_status = 'EKS';
        """;

    private const string FetchSourceDataHierarchyQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT
            CAST(kods as text)                          AS Code,
            nosaukums                                   AS DisplayName,
            CASE WHEN vkur_tips = 104 THEN vkur_cd END  AS CityCode,
            CASE WHEN vkur_tips = 105 THEN vkur_cd END  AS ParishCode,
            CASE WHEN vkur_tips = 106 THEN vkur_cd END  AS VillageCode,
            CASE WHEN vkur_tips = 107 THEN vkur_cd END  AS StreetCode,
            CASE WHEN vkur_tips = 113 THEN vkur_cd END  AS CountyCode
        FROM varis.iedalijums
        WHERE statuss = 'EKS' AND tips_cd IN (105, 106, 107);
        """;

    #endregion

    #region MSSQL fetch queries

    private const string FetchDestinationDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT Id, Code, DisplayName, VillageCode, CityCode, StreetCode, ParishCode, CountyCode FROM [GIS].[{0}] WITH (NOLOCK) WHERE IsDeleted = 0;
        """;

    #endregion

    #region Update queries

    private const string MarkForDeleteQuery =
        """
        -- noinspection SqlResolveForStatement
        UPDATE [GIS].[{0}] SET IsDeleted = 1 WHERE Id IN @id;
        """;

    private const string SwitchAliasQuery =
        """
        -- noinspection SqlResolveForStatement
        DROP SYNONYM [GIS].[CLS_Building]; CREATE SYNONYM [GIS].[CLS_Building] FOR [GIS].[{0}];
        """;

    private const string BulkInsertClassifiersQuery =
        """
        -- noinspection SqlResolveForStatement
        INSERT INTO [GIS].[{0}] (Code, DisplayName, CityCode, VillageCode, StreetCode, CountyCode, ParishCode, Language, IsDeleted) VALUES
        """;

    private const string UpdateClassifierQuery =
        """
        -- noinspection SqlResolveForStatement
        UPDATE [GIS].[{0}] SET DisplayName = @DisplayName, CityCode = @CityCode, VillageCode = @VillageCode, StreetCode = @StreetCode, CountyCode = @CountyCode, ParishCode = @ParishCode WHERE Id = @Id;
        """;

    #endregion

    public IEnumerable<BuildingClassifier> GetSourceClassifiers() {
        using var connection = this.CreateOpenSourceConnection();
        var records = connection.Query<BuildingClassifier>(BuildingRepository.FetchSourceDataQuery);

        return records;
    }

    public IEnumerable<BuildingClassifier> GetDestinationClassifiers(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        var records = connection.Query<BuildingClassifier>(string.Format(BuildingRepository.FetchDestinationDataQuery, table));
        
        return records;
    }

    public IEnumerable<BuildingClassifier> GetHierarchy()
    {
        using var connection = this.CreateOpenSourceConnection();
        var records = connection.Query<BuildingClassifier>(BuildingRepository.FetchSourceDataHierarchyQuery);
        
        return records;
    }
    
    public void MarkForDelete(string table, object @params) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(BuildingRepository.MarkForDeleteQuery, table), @params);
    }

    public void SwitchAliasTo(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(BuildingRepository.SwitchAliasQuery, table));
    }

    public void BulkInsertClassifiers(IReadOnlyCollection<BuildingClassifier> classifiers, string table)
    {
        if (!classifiers.Any()) return;

        var query = QueryBuilder.BuildGetUsersQuery(classifiers, string.Format(BuildingRepository.BulkInsertClassifiersQuery, table));
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(query);
    }

    public void UpdateClassifier(BuildingClassifier? classifier, string table)
    {
        if (classifier is null) return;
        
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(BuildingRepository.UpdateClassifierQuery, table), classifier);
    }

    private static class QueryBuilder
    {
        public static string BuildGetUsersQuery(IReadOnlyCollection<BuildingClassifier> classifiers, string query)
        {
            var values = string.Join(",", classifiers.Select(data =>
                $"({data.Code.ToNullOrEscape()}, " +
                $"{data.DisplayName.ToNullOrEscape()}, " +
                $"{data.CityCode.ToNullOrEscape()}, " +
                $"{data.VillageCode.ToNullOrEscape()}, " +
                $"{data.StreetCode.ToNullOrEscape()}, " +
                $"{data.CountyCode.ToNullOrEscape()}, " +
                $"{data.ParishCode.ToNullOrEscape()}, " +
                $"1, " +
                $"0)\n"));
  
            return query + values;
        }
    }
}