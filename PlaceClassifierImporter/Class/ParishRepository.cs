﻿using Dapper;
using PlaceClassifierImporter.Entity;
using TaskCore;

namespace PlaceClassifierImporter.Class;

public class ParishRepository : Repository, IClassifierRepository<ParishClassifier>
{
    public ParishRepository(string sourceConnectionString, string destinationConnectionString): base(sourceConnectionString, destinationConnectionString) {}

    #region Postgres fetch queries

    private const string FetchSourceDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT CAST(kods as text) AS Code, nosaukums AS DisplayName, CASE WHEN vkur_tips = 113 THEN vkur_cd END AS CountyCode
        FROM varis.iedalijums
        WHERE tips_cd = 105
          AND statuss = 'EKS'
        """;

    #endregion

    #region MSSQL fetch queries

    private const string FetchDestinationDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT Id, Code, DisplayName, CountyCode FROM [GIS].[{0}] WITH (NOLOCK) WHERE IsDeleted = 0;
        """;

    #endregion

    #region Update queries

    private const string MarkForDeleteQuery =
        """
        -- noinspection SqlResolveForStatement
        UPDATE [GIS].[{0}] SET IsDeleted = 1 WHERE Id IN @id;
        """;

    private const string SwitchAliasQuery =
        """
        -- noinspection SqlResolveForStatement
        DROP SYNONYM [GIS].[CLS_Parish]; CREATE SYNONYM [GIS].[CLS_Parish] FOR [GIS].[{0}];
        """;

    private const string BulkInsertClassifiersQuery =
        """
        -- noinspection SqlResolveForStatement
        INSERT INTO [GIS].[{0}] (Code, DisplayName, CountyCode, Language, IsDeleted) VALUES
        """;

    private const string UpdateClassifierQuery =
        """
        -- noinspection SqlResolveForStatement
        UPDATE [GIS].[{0}] SET DisplayName = @DisplayName, CountyCode = @CountyCode WHERE Id = @Id;
        """;

    #endregion

    public IEnumerable<ParishClassifier> GetSourceClassifiers() {
        using var connection = this.CreateOpenSourceConnection();
        var records = connection.Query<ParishClassifier>(ParishRepository.FetchSourceDataQuery);

        return records;
    }

    public IEnumerable<ParishClassifier> GetDestinationClassifiers(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        var records = connection.Query<ParishClassifier>(string.Format(ParishRepository.FetchDestinationDataQuery, table));
        
        return records;
    }

    public IEnumerable<ParishClassifier> GetHierarchy()
    {
        return new List<ParishClassifier>();
    }
    
    public void MarkForDelete(string table, object @params) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(ParishRepository.MarkForDeleteQuery, table), @params);
    }

    public void SwitchAliasTo(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(ParishRepository.SwitchAliasQuery, table));
    }

    public void BulkInsertClassifiers(IReadOnlyCollection<ParishClassifier> classifiers, string table)
    {
        if (!classifiers.Any()) return;

        var query = QueryBuilder.BuildGetUsersQuery(classifiers, string.Format(ParishRepository.BulkInsertClassifiersQuery, table));
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(query);
    }

    public void UpdateClassifier(ParishClassifier? classifier, string table)
    {
        if (classifier is null) return;
        
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(ParishRepository.UpdateClassifierQuery, table), classifier);
    }

    private static class QueryBuilder
    {
        public static string BuildGetUsersQuery(IReadOnlyCollection<ParishClassifier> classifiers, string query)
        {
            var values = string.Join(",", classifiers.Select(data =>
                $"({data.Code.ToNullOrEscape()}, " +
                $"{data.DisplayName.ToNullOrEscape()}, " +
                $"{data.CountyCode.ToNullOrEscape()}, " +
                $"1, " +
                $"0)\n"));
  
            return query + values;
        }
    }
}