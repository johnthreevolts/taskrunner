﻿using Dapper;
using PlaceClassifierImporter.Entity;
using TaskCore;

namespace PlaceClassifierImporter.Class;

public class CityRepository : Repository, IClassifierRepository<CityClassifier>
{
    public CityRepository(string sourceConnectionString, string destinationConnectionString): base(sourceConnectionString, destinationConnectionString) {}

    #region Postgres fetch queries

    private const string FetchSourceDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT 
            CAST(kods as text)                          AS Code,
            nosaukums                                   AS DisplayName,
            CASE WHEN vkur_tips = 101 THEN 1 ELSE 0 END AS IsGeneralCity,
            CASE WHEN vkur_tips = 113 THEN vkur_cd END  AS CountyCode
        FROM varis.iedalijums
        WHERE tips_cd = 104
        AND statuss = 'EKS';
        """;

    #endregion

    #region MSSQL fetch queries

    private const string FetchDestinationDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT Id, Code, DisplayName, CountyCode, IsGeneralCity FROM [GIS].[{0}] WITH (NOLOCK) WHERE IsDeleted = 0;
        """;

    #endregion

    #region Update queries

    private const string MarkForDeleteQuery =
        """
        -- noinspection SqlResolveForStatement
        UPDATE [GIS].[{0}] SET IsDeleted = 1 WHERE Id IN @id;
        """;

    private const string SwitchAliasQuery =
        """
        -- noinspection SqlResolveForStatement
        DROP SYNONYM [GIS].[CLS_City]; CREATE SYNONYM [GIS].[CLS_City] FOR [GIS].[{0}];
        """;

    private const string BulkInsertClassifiersQuery =
        """
        -- noinspection SqlResolveForStatement
        INSERT INTO [GIS].[{0}] (Code, DisplayName, CountyCode, IsGeneralCity, Language, IsDeleted) VALUES
        """;

    private const string UpdateClassifierQuery =
        """
        -- noinspection SqlResolveForStatement
        UPDATE [GIS].[{0}] SET DisplayName = @DisplayName, CountyCode = @CountyCode, IsGeneralCity = @IsGeneralCity WHERE Id = @Id;
        """;

    #endregion

    public IEnumerable<CityClassifier> GetSourceClassifiers() {
        using var connection = this.CreateOpenSourceConnection();
        var records = connection.Query<CityClassifier>(CityRepository.FetchSourceDataQuery);

        return records;
    }

    public IEnumerable<CityClassifier> GetDestinationClassifiers(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        var records = connection.Query<CityClassifier>(string.Format(CityRepository.FetchDestinationDataQuery, table));
        
        return records;
    }

    public IEnumerable<CityClassifier> GetHierarchy()
    {
        return new List<CityClassifier>();
    }
    
    public void MarkForDelete(string table, object @params) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(CityRepository.MarkForDeleteQuery, table), @params);
    }

    public void SwitchAliasTo(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(CityRepository.SwitchAliasQuery, table));
    }

    public void BulkInsertClassifiers(IReadOnlyCollection<CityClassifier> classifiers, string table)
    {
        if (!classifiers.Any()) return;

        var query = QueryBuilder.BuildGetUsersQuery(classifiers, string.Format(CityRepository.BulkInsertClassifiersQuery, table));
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(query);
    }

    public void UpdateClassifier(CityClassifier? classifier, string table)
    {
        if (classifier is null) return;
        
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(CityRepository.UpdateClassifierQuery, table), classifier);
    }
    
    private static class QueryBuilder
    {
        public static string BuildGetUsersQuery(IReadOnlyCollection<CityClassifier> classifiers, string query)
        {
            var values = string.Join(",", classifiers.Select(data =>
                $"({data.Code.ToNullOrEscape()}, " +
                $"{data.DisplayName.ToNullOrEscape()}, " +
                $"{data.CountyCode.ToNullOrEscape()}, " +
                $"{(data.IsGeneralCity ? 1 : 0)}, " +
                $"1, " +
                $"0)\n"));
  
            return query + values;
        }
    }
}
