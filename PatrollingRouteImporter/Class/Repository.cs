﻿using System.Data;
using System.Data.SqlClient;
using Dapper;
using Npgsql;
using PatrollingRouteImporter.Record;
using TaskCore;

namespace PatrollingRouteImporter.Class;

public class Repository
{
    private readonly string _sourceConnectionString;
    private readonly string _destinationConnectionString;

    public Repository(string sourceConnectionString, string destinationConnectionString)
    {
        this._sourceConnectionString = sourceConnectionString;
        this._destinationConnectionString = destinationConnectionString;
    }

    #region Postgres fetch queries

    private const string FetchSourceDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT id                           AS RouteId,
               properties ->> 'NOSAUKUMS'   AS Name,
               deleted_at                   AS DeletedAt
        FROM public.map_objects
        WHERE object_group_id = 22;
        """;

    #endregion

    #region MSSQL fetch queries

    private const string FetchDestinationDataQuery =
        """
        -- noinspection SqlResolveForStatement
        SELECT Id, RouteId, Name, DeletedAt FROM [GIS].[{0}];
        """;

    #endregion

    #region Update queries

    private const string DeleteQuery =
        """
        -- noinspection SqlResolveForStatement
        DELETE FROM [GIS].[{0}] WHERE Id IN @id;
        """;

    private const string SwitchAliasQuery =
        """
        -- noinspection SqlResolveForStatement
        DROP SYNONYM [GIS].[PatrollingRoute]; CREATE SYNONYM [GIS].[PatrollingRoute] FOR [GIS].[{0}];
        """;

    private const string BulkInsertClassifiersQuery =
        """
        -- noinspection SqlResolveForStatement
        INSERT INTO [GIS].[{0}] (RouteId, Name, DeletedAt) VALUES
        """;

    private const string UpdateClassifierQuery =
        """
        -- noinspection SqlResolveForStatement
        UPDATE [GIS].[{0}] SET Name = @Name, DeletedAt = @DeletedAt WHERE RouteId = @RouteId;
        """;

    #endregion

    public IEnumerable<PatrollingRoute> GetSourceClassifiers() {
        using var connection = this.CreateOpenSourceConnection();
        var records = connection.Query<PatrollingRoute>(Repository.FetchSourceDataQuery);

        return records;
    }

    public IEnumerable<PatrollingRoute> GetDestinationClassifiers(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        var records = connection.Query<PatrollingRoute>(string.Format(Repository.FetchDestinationDataQuery, table));
        
        return records;
    }

    public void Delete(string table, object @params) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(Repository.DeleteQuery, table), @params);
    }

    public void SwitchAliasTo(string table) {
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(Repository.SwitchAliasQuery, table));
    }

    public void BulkInsertClassifiers(IReadOnlyCollection<PatrollingRoute> classifiers, string table)
    {
        if (!classifiers.Any()) return;

        var query = QueryBuilder.BuildGetUsersQuery(classifiers, string.Format(Repository.BulkInsertClassifiersQuery, table));
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(query);
    }

    public void UpdateClassifier(PatrollingRoute? classifier, string table)
    {
        if (classifier is null) return;
        
        using var connection = this.CreateOpenDestinationConnection();
        connection.Execute(string.Format(Repository.UpdateClassifierQuery, table), classifier);
    }

    private IDbConnection CreateOpenSourceConnection()
    {
        var connection = new NpgsqlConnection(this._sourceConnectionString);
        connection.Open();
        return connection;
    }

    private IDbConnection CreateOpenDestinationConnection()
    {
        var connection = new SqlConnection(this._destinationConnectionString);
        connection.Open();
        return connection;
    }

    private static class QueryBuilder
    {
        public static string BuildGetUsersQuery(IReadOnlyCollection<PatrollingRoute> classifiers, string query)
        {
            var values = string.Join(",", classifiers.Select(data =>
                $"({data.RouteId}, " +
                $"{data.Name.ToNullOrEscape(false)}, " +
                $"{data.DeletedAt.ToNullOrEscape()})\n"));
  
            return query + values;
        }
    }
}