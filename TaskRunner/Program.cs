using TaskCore;
using System.CommandLine;
using System.Text;

Console.OutputEncoding = Encoding.UTF8;

Action<string> writer = Console.WriteLine;
var registeredTasks = new Dictionary<string, Runnable>();

#region Task registration
RegisterTask(new PlaceClassifierImporter.Payload());
RegisterTask(new VkcpNmpdEmployeeToVehicleTransfer.Payload());
RegisterTask(new PatrollingRouteImporter.Payload());
RegisterTask(new OperativeServiceImporter.Payload());
#endregion

var option = CreateTaskOption();
var command = new RootCommand { option };
command.TreatUnmatchedTokensAsErrors = false;
command.SetHandler(async (taskName) =>
    {
        if (!registeredTasks.TryGetValue(taskName, out var runnable)) throw new Exception("Task not found");

        var mutex = new Mutex(true, taskName, out var createdNew);
        if (createdNew)
        {
            var commands = runnable.GetCommands(option);
            writer($"🚀 Starting {taskName} payload!" + Environment.NewLine);
            using (mutex)
            {
                await commands.InvokeAsync(args);
                writer($"✅  {taskName} done.");
            }
        }
        else
        {
            writer("Task is already running, please try later.");
        }
    },
    option);
await command.InvokeAsync(args);

#region Functions
void RegisterTask<TRunnable>(TRunnable task) where TRunnable : Runnable
{
    registeredTasks.Add(task.PayloadName, task);
}

Option<string> CreateTaskOption()
{
    var taskOption = Helper.CreateOption<string>(
        "--task",
        "-t",
        "TR_Task",
        "Name of task to run");
    taskOption.FromAmong(new List<string>(registeredTasks.Keys).ToArray());
    return taskOption;
}
#endregion