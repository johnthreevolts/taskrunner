﻿using System.CommandLine;

namespace TaskCore;

public static class Helper
{
    private const string TaskFailed = "💥 Task failed.";
    
    private static bool TryGetParamFromEnv(string name, out string param)
    {
        var result = Environment.GetEnvironmentVariable(name);
        if (result is not null)
        {
            param = result;
            return true;
        }
        param = string.Empty;
        return false;
    }

    public static Option<TType> CreateOption<TType>(string name, string alias, string envName, string description, bool isRequired = true)
    {
        var hasParam = Helper.TryGetParamFromEnv(envName, out var param);

        var option = new Option<TType>(name: name, description: description);
        option.AddAlias(alias);
        option.IsRequired = isRequired;
        if (hasParam)
        {
            option.SetDefaultValue(param);
        }

        return option;
    }

    public static Action RunOrLog(Action action)
    {
        return () => Helper.RunOrLogInternal(action);
    }

    public static Action<T1> RunOrLog<T1>(Action<T1> action)
    {
        return param1 => Helper.RunOrLogInternal(() => action.Invoke(param1));
    }

    public static Action<T1, T2> RunOrLog<T1, T2>(Action<T1, T2> action)
    {
        return (param1, param2) => Helper.RunOrLogInternal(() => action.Invoke(param1, param2));
    }

    public static Action<T1, T2, T3> RunOrLog<T1, T2, T3>(Action<T1, T2, T3> action)
    {
        return (param1, param2, param3) => Helper.RunOrLogInternal(() => action.Invoke(param1, param2, param3));
    }

    public static Action<T1, T2, T3, T4> RunOrLog<T1, T2, T3, T4>(Action<T1, T2, T3, T4> action)
    {
        return (param1, param2, param3, param4) => Helper.RunOrLogInternal(() => action.Invoke(param1, param2, param3, param4));
    }
    
    public static Func<Task> RunOrLogAsync(Func<Task> asyncAction)
    {
        return async () => await Helper.RunOrLogInternalAsync(async () => await asyncAction.Invoke());
    }
    public static Func<T1, Task> RunOrLogAsync<T1>(Func<T1, Task> asyncAction)
    {
        return async (param1) => await Helper.RunOrLogInternalAsync(async () => await asyncAction.Invoke(param1));
    }
    public static Func<T1, T2, Task> RunOrLogAsync<T1, T2>(Func<T1, T2, Task> asyncAction)
    {
        return async (param1, param2) => await Helper.RunOrLogInternalAsync(async () => await asyncAction.Invoke(param1, param2));
    }
    public static Func<T1, T2, T3, Task> RunOrLogAsync<T1, T2, T3>(Func<T1, T2, T3, Task> asyncAction)
    {
        return async (param1, param2, param3) => await Helper.RunOrLogInternalAsync(async () => await asyncAction.Invoke(param1, param2, param3));
    }
    public static Func<T1, T2, T3, T4, Task> RunOrLogAsync<T1, T2, T3, T4>(Func<T1, T2, T3, T4, Task> asyncAction)
    {
        return async (param1, param2, param3, param4) => await Helper.RunOrLogInternalAsync(async () => await asyncAction.Invoke(param1, param2, param3, param4));
    }

    private static void RunOrLogInternal(Action action)
    {
        try
        {
            action.Invoke();
        }
        catch (GracefulException ge)
        {
            if (ge.HasMessage)
            {
                Console.WriteLine(ge.Message + Environment.NewLine);
            }
            Console.WriteLine(Helper.TaskFailed);
            Environment.Exit(-1);
        }
        catch (Exception e)
        {
            Console.WriteLine(Helper.TaskFailed);
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
            Environment.Exit(-1);
        }
    }

    private static async Task RunOrLogInternalAsync(Func<Task> action)
    {
        try
        {
            await action();
        }
        catch (GracefulException ge)
        {
            if (ge.HasMessage)
            {
                Console.WriteLine(ge.Message + Environment.NewLine);
            }
            Console.WriteLine(Helper.TaskFailed);
            Environment.Exit(-1);
        }
        catch (Exception e)
        {
            Console.WriteLine(Helper.TaskFailed + Environment.NewLine);
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
            Environment.Exit(-1);
        }
    }
    
    public static string ToNullOrEscape(this string? value, bool threatEmptyAsNull = true)
    {
        return value is null || (threatEmptyAsNull && value == "")
            ? "NULL"
            : $"N'{value}'";
    }
    
    public static string ToNullOrEscape(this DateTime? value)
    {
        return value is null
            ? "NULL"
            : $"N'{value.ToString()}'";
    }
}